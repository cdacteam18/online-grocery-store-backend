package com.springrest.pojos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


@Entity
@Table(name = "addresses")
public class Address extends BaseEntity {

	@Column(name="address_line_1",length = 45)
	private String addressLine1;
	
	@Column(name="address_line_2",length = 45)
	private String addressLine2;
	
	@Column(length = 30)
	private String City;
	
	@Column(length = 10)
	private String pinCode;
	
	@Column(length = 30)
	private String state;
	
	@Column(length = 30)
	private String country;

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public String toString() {
		return "Address [addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", City=" + City
				+ ", pinCode=" + pinCode + ", state=" + state + ", country=" + country + "]";
	}
	
	
	
}
