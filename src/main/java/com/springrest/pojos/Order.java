package com.springrest.pojos;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table
public class Order extends BaseEntity {

	@Column(name="total_price")
	private double totalPrice;
	
	@DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
	@Column(name="order_date")
	private LocalDateTime orderstatus;
	
	@Column(length=25,name="order_status")
	private String orderStatus;
	
	@DateTimeFormat(pattern="yyyy-MM-dd hh:mm:ss")
	@Column(name="status_update_date")
	private LocalDateTime statusUpdateDate;
	
	
	private Address deliveryAddress;
	
	private User Customer;

	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Order(double totalPrice, LocalDateTime orderstatus, String orderStatus2, LocalDateTime statusUpdateDate,
			Address deliveryAddress, User customer) {
		super();
		this.totalPrice = totalPrice;
		this.orderstatus = orderstatus;
		orderStatus = orderStatus2;
		this.statusUpdateDate = statusUpdateDate;
		this.deliveryAddress = deliveryAddress;
		Customer = customer;
	}

	public double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public LocalDateTime getOrderstatus() {
		return orderstatus;
	}

	public void setOrderstatus(LocalDateTime orderstatus) {
		this.orderstatus = orderstatus;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public LocalDateTime getStatusUpdateDate() {
		return statusUpdateDate;
	}

	public void setStatusUpdateDate(LocalDateTime statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}

	public Address getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public User getCustomer() {
		return Customer;
	}

	public void setCustomer(User customer) {
		Customer = customer;
	}

	@Override
	public String toString() {
		return "Order [totalPrice=" + totalPrice + ", orderstatus=" + orderstatus + ", orderStatus=" + orderStatus
				+ ", statusUpdateDate=" + statusUpdateDate + ", deliveryAddress=" + deliveryAddress + ", Customer="
				+ Customer + "]";
	}
	
	
	
	
	
	
	
}
